<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories\Todo;
use App\User;

/**
 * Description of TodoRepositories
 *
 * @author User
 */
class EloquentTodo implements TodoRepositories {
    //put your code here
    private $model;
    
    public function __construct(User $model) {
        $this->model = $model;
    }
    public function getAll(){
        return $this->model->all();
    }
}
