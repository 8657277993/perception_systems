<?php

namespace App\Providers;

use App\Repositories\Todo\EloquentTodo;
use App\Repositories\Todo\TodoRepositories;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton(TodoRepositories::class, EloquentTodo::class); 
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
