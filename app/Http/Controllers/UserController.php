<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Todo\TodoRepositories;

class UserController extends Controller
{
    private $todo; 
    
    public function __construct(TodoRepositories $todo) {
        $this->todo = $todo;
    }
    
    public function index() {
        $this->viewData['pageTitle'] = '';
        $this->viewData['data'] = $this->todo->getAll();
        return view('dashboard', $this->viewData);
    }
    

}
