<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class userTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // First Way to insrting the data;
        //        User::create(
//                [
//                    'firstname'=>'Shrikant',
//                    'lastname'=>'Dhanvijay',
//                    'mobile'=>rand(1000000000,9999999999),
//                    'email'=>'shrikant@gmail.com',
//                    'age'=>rand(10,100),
//                    'gender'=>'m',
//                    'city'=>'nagpur',
//                    'password'=>bcrypt("12345"),
//                ]);
//        
//        User::create([
//                    'firstname'=>'Shrikant',
//                    'lastname'=>'Dhanvijay',
//                    'mobile'=>rand(1000000000,9999999999),
//                    'email'=>'sd@gmail.com',
//                    'age'=>rand(10,100),
//                    'gender'=>'m',
//                    'city'=>'nagpur',
//                    'password'=>bcrypt("12345"),
//                ]);
        
        // Secod Way to inserting the data;
        $data = [
                [
                    'firstname'=>'Shrikant',
                    'lastname'=>'Dhanvijay',
                    'mobile'=>rand(1000000000,9999999999),
                    'email'=>'shrikant@gmail.com',
                    'age'=>rand(10,100),
                    'gender'=>'m',
                    'city'=>'nagpur',
                    'password'=>bcrypt("12345"),
                ],
                [
                    'firstname'=>'Amol',
                    'lastname'=>'Kharate',
                    'mobile'=>rand(1000000000,9999999999),
                    'email'=>'ak@gmail.com',
                    'age'=>rand(10,100),
                    'gender'=>'m',
                    'city'=>'nagpur',
                    'password'=>bcrypt("12345"),
                ],
                [
                    'firstname'=>'Shubham',
                    'lastname'=>'Narnawre',
                    'mobile'=>rand(1000000000,9999999999),
                    'email'=>'sn@gmail.com',
                    'age'=>rand(10,100),
                    'gender'=>'m',
                    'city'=>'nagpur',
                    'password'=>bcrypt("12345"),
                ],
                [
                    'firstname'=>'Kalpna',
                    'lastname'=>'Anyarao',
                    'mobile'=>rand(1000000000,9999999999),
                    'email'=>'ka@gmail.com',
                    'age'=>rand(10,100),
                    'gender'=>'f',
                    'city'=>'nagpur',
                    'password'=>bcrypt("12345"),
                ],
                [
                    'firstname'=>'Shreya',
                    'lastname'=>'Bhai',
                    'mobile'=>rand(1000000000,9999999999),
                    'email'=>'sb@gmail.com',
                    'age'=>rand(10,100),
                    'gender'=>'f',
                    'city'=>'nagpur',
                    'password'=>bcrypt("12345"),
                ],
                [
                    'firstname'=>'Pawan',
                    'lastname'=>'Ramteke',
                    'mobile'=>rand(1000000000,9999999999),
                    'email'=>'pr@gmail.com',
                    'age'=>rand(10,100),
                    'gender'=>'m',
                    'city'=>'nagpur',
                    'password'=>bcrypt("12345"),
                ],
                [
                    'firstname'=>'Amol',
                    'lastname'=>'Hirkane',
                    'mobile'=>rand(1000000000,9999999999),
                    'email'=>'ah@gmail.com',
                    'age'=>rand(10,100),
                    'gender'=>'m',
                    'city'=>'nagpur',
                    'password'=>bcrypt("12345"),
                ],
                [
                    'firstname'=>'Ravi',
                    'lastname'=>'Dahiwale',
                    'mobile'=>rand(1000000000,9999999999),
                    'email'=>'rd@gmail.com',
                    'age'=>rand(10,100),
                    'gender'=>'m',
                    'city'=>'nagpur',
                    'password'=>bcrypt("12345"),
                ],
                [
                    'firstname'=>'Mayur',
                    'lastname'=>'Rangari',
                    'mobile'=>rand(1000000000,9999999999),
                    'email'=>'mr@gmail.com',
                    'age'=>rand(10,100),
                    'gender'=>'m',
                    'city'=>'nagpur',
                    'password'=>bcrypt("12345"),
                ],
                [
                    'firstname'=>'Sanjana',
                    'lastname'=>'Shahu',
                    'mobile'=>rand(1000000000,9999999999),
                    'email'=>'ss@gmail.com',
                    'age'=>rand(10,100),
                    'gender'=>'f',
                    'city'=>'nagpur',
                    'password'=>bcrypt("12345"),
                ],
                [
                    'firstname'=>'Ashwini',
                    'lastname'=>'Rokade',
                    'mobile'=>rand(1000000000,9999999999),
                    'email'=>'ar@gmail.com',
                    'age'=>rand(10,100),
                    'gender'=>'f',
                    'city'=>'nagpur',
                    'password'=>bcrypt("12345"),
                ],
                [
                    'firstname'=>'Rahul',
                    'lastname'=>'Rokade',
                    'mobile'=>rand(1000000000,9999999999),
                    'email'=>'rr@gmail.com',
                    'age'=>rand(10,100),
                    'gender'=>'m',
                    'city'=>'nagpur',
                    'password'=>bcrypt("12345"),
                ],
                [
                    'firstname'=>'Raj',
                    'lastname'=>'Mandarkar',
                    'mobile'=>rand(1000000000,9999999999),
                    'email'=>'rm@gmail.com',
                    'age'=>rand(10,100),
                    'gender'=>'m',
                    'city'=>'nagpur',
                    'password'=>bcrypt("12345"),
                ],
                [
                    'firstname'=>'Ashish',
                    'lastname'=>'Hardiya',
                    'mobile'=>rand(1000000000,9999999999),
                    'email'=>'ahe@gmail.com',
                    'age'=>rand(10,100),
                    'gender'=>'m',
                    'city'=>'nagpur',
                    'password'=>bcrypt("12345"),
                ],
                [
                    'firstname'=>'Rohit',
                    'lastname'=>'Maske',
                    'mobile'=>rand(1000000000,9999999999),
                    'email'=>'rms@gmail.com',
                    'age'=>rand(10,100),
                    'gender'=>'m',
                    'city'=>'nagpur',
                    'password'=>bcrypt("12345"),
                ]
            ];
        User::insert($data);
        

    }
}
