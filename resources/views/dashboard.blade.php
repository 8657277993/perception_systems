@extends('layouts.admin')
@section('page-content')
<div class="content-wrapper">
    <!--<section class="content-header">
        <h1>Data Tables<small>advanced tables</small></h1>
    </section>-->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                 <div class="hide" id="success_msg">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="fa fa-times"></i>
                    </button>
                    <p id="success_response_msg"><i class="icon fa fa-check"></i></p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="box  border-top-none">
                    <div class="box-header with-border">
                        <h3 class="box-title">User List</h3>
                        <div class="box-tools pull-right">
                            <!--<a href="#" class="btn btn-sm btn-info btn-flat pull-right">&nbsp;</a>-->
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="box-body table-responsive no-padding">
                            <div class="box-header">
                                <div class="card-body">
                                    <table id="example" class="display" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Mobile</th>
                                                <th>Email</th>
                                                <th>Age</th>
                                                <th>Gender</th>
                                                <th>City</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data as $row)
                                                <tr>
                                                    <th>{{$row->firstname}}</th>
                                                    <th>{{$row->lastname}}</th>
                                                    <th>{{$row->mobile}}</th>
                                                    <th>{{$row->email}}</th>
                                                    <th>{{$row->age}}</th>
                                                    <th>{{$row->gender}}</th>
                                                    <th>{{$row->city}}</th>
                                                </tr>
                                            @endforeach;
                                        </tbody>
                                        <tfoot>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                             <th>Mobile</th>
                                            <th>Email</th>
                                            <th>Age</th>
                                            <th>Gender</th>
                                            <th>City</th>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">

$(document).ready(function() {
    $('#example').DataTable( {
        "order": [[ 1, "desc" ]]
    } );
} );
</script>
@endsection 